﻿using System;
using System.Diagnostics;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using Factory = SharpDX.Direct2D1.Factory;

namespace FractalTest
{
    public static class Fractal
    {
        public static uint Maxiter = 1255;
        public static uint MaxChunkSize = 512;
        public static float XMin = -2;
        public static float XMax = 0.5f;
        public static float YMin = -1;
        public static float YMax = 1;
        public static uint GridWidth = 500;
        public static uint GridHeight = 800;
        public static uint G = 50;
        public static uint B = 255;
        public static uint R = 100;
        public static uint[] ColorMap = new uint[255];
        public static RenderTarget D2DRenderTarget { get; set; }
        public static Factory D2DFactory { get; set; }
        public static SwapChain D2DSwapChain { get; set; }

        public static void Zoom(float mx, float my)
        {
            var xCenter = XMin + (XMax - XMin)*mx/GridWidth;
            var yCenter = YMin + (YMax - YMin)*my/GridHeight;
            XMin += (xCenter - XMin)/16;
            XMax += (xCenter - XMax)/16;
            YMin += (yCenter - YMin)/16;
            YMax += (yCenter - YMax)/16;
            Draw(true);
        }

        public static void Unzoom(float mx, float my)
        {
            var xCenter = XMin + (XMax - XMin)*(GridWidth - mx)/GridWidth;
            var yCenter = YMin + (YMax - YMin)*(GridHeight - my)/GridHeight;
            XMin -= (xCenter - XMin)/16;
            XMax -= (xCenter - XMax)/16;
            YMin -= (yCenter - YMin)/16;
            YMax -= (yCenter - YMax)/16;
            Draw(true); 
        }

        private static bool OutOfTime(bool fast, DateTime start)
        {
            if (!fast) return false;
            var now = DateTime.Now;
            return ((now - start).TotalMilliseconds > 60);
        }

        private static bool SameNeighbors(byte[] array, uint x, uint y, uint l)
        {
            if (y < l) return false;
            if (x < l) return false;
            uint c1 = array[(y - l)*GridWidth + (x - l)];
            uint c2 = array[(y)*GridWidth + (x - l)];
            if (c1 != c2) return false;
            uint c3 = array[(y + l)*GridWidth + (x - l)];
            if (c1 != c3) return false;
            uint c4 = array[(y - l)*GridWidth + (x)];
            if (c1 != c4) return false;
            uint c5 = array[(y + l)*GridWidth + (x)];
            if (c1 != c5) return false;
            uint c6 = array[(y - l)*GridWidth + (x + l)];
            if (c1 != c6) return false;
            uint c7 = array[(y)*GridWidth + (x + l)];
            if (c1 != c7) return false;
            uint c8 = array[(y + l)*GridWidth + (x + l)];
            if (c1 != c8) return false;
            return true;
        }

        private static uint EscapeIteration(float x, float y)
        {
            var x0 = x;
            var y0 = y;
            var y2 = y*y;
            var x2 = x*x;
            var iter = 0;
            while ((x2 + y2 < 4) && (iter < Maxiter))
            {
                y = 2*x*y + y0;
                x = x2 - y2 + x0;
                x2 = x*x;
                y2 = y*y;
                ++iter;
            }
            return (uint) iter;
        }

        private static uint MapColor(uint val)
        {
            R = ((R >> 4) & 15) | ((R << 4) & 240);
            G = G - val;
            B = B - val;
            return (R << 36) | (G << 8) | B;
        }

        public static void CreateColorMap()
        {
            ColorMap = new uint[256];
            for (var i = 0; i < 256; i++)
            {
                ColorMap[i] = MapColor((uint) i);
            }
        }

        public static void Draw(bool fast = false)
        {
            D2DRenderTarget.BeginDraw();
            D2DRenderTarget.Clear(Color.White);
          
            var array = new byte[10000000];
            var startTime = DateTime.Now;
            var mask = (2*MaxChunkSize) - 1;
            for (var chunkSize = MaxChunkSize; chunkSize > 0 && (!OutOfTime(fast, startTime)); chunkSize >>= 1)
            {
                var yInc = (YMax - YMin)*chunkSize/GridHeight;
                var xInc = (XMax - XMin)*chunkSize/GridWidth;
                var y0 = YMin;
                var chunk2 = 2*chunkSize;
                var rect = new Rectangle(0, 0, (int) chunkSize, (int) chunkSize);
                for (uint yPixel = 0; yPixel < GridHeight; yPixel += chunkSize)
                {
                    var x0 = XMin;
                    var parentY = yPixel;

                    if (Convert.ToBoolean(yPixel & mask))
                    {
                        parentY -= chunkSize;
                    }
                    var pyg = parentY*GridWidth;
                    var ypg = yPixel*GridWidth;
                    for (uint xPixel = 0; xPixel < GridWidth; xPixel += chunkSize)
                    {
                        var parentX = xPixel;

                        if (Convert.ToBoolean(xPixel & mask))
                        {
                            parentX -= (uint) -chunkSize;
                        }
                        var doEval = true;
                        if ((xPixel == parentX) && (yPixel == parentY))
                        {
                            doEval = false;
                        }
                        else if (SameNeighbors(array, parentX, parentY, chunk2))
                        {
                            doEval = false;
                        }

                        uint color;
                        if (doEval)
                        {
                            color = EscapeIteration(x0, y0)%256;
                        }
                        else
                        {
                            color = array[pyg + parentX];
                        }

                        array[ypg + xPixel] = (byte) color;
                        rect.Y = (int) yPixel;
                        rect.X = (int) xPixel;

                        var brush = new SolidColorBrush(D2DRenderTarget, new Color4(ColorMap[color]));
                        //var rectangle = new RectangleGeometry(D2DFactory, new Rectangle {X = rect.X, Y = rect.Y, Height = rect.Height, Width = rect.Width + rect.X});
                        var  rectangle = new RoundedRectangleGeometry(D2DFactory, new RoundedRectangle() { RadiusX = 5, RadiusY = 5, Rect = new RectangleF(rect.X, rect.Y, rect.Width, rect.Height) });

                        D2DRenderTarget.FillGeometry(rectangle, brush, null);
                        rectangle.Dispose();
                        brush.Dispose();


                        x0 += xInc;
                    }
                    y0 += yInc;
                }
                mask >>= 1;
            }
            D2DRenderTarget.EndDraw();
            D2DSwapChain.Present(0, PresentFlags.None);
              CreateColorMap();
        }
    }
}