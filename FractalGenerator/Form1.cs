﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;

using AlphaMode = SharpDX.Direct2D1.AlphaMode;
using Device = SharpDX.Direct3D11.Device;
using Factory = SharpDX.DXGI.Factory;
namespace FractalGenerator
{
    public partial class Form1 : Form
    {
        static RenderForm form;
        static SwapChainDescription desc;
        static Device device;
        static SwapChain swapChain;
        static SharpDX.Direct2D1.Factory d2dFactory;
        static Factory factory;
        static Texture2D backBuffer;
        static RenderTargetView renderView;
        static Surface surface;
        static RenderTarget d2dRenderTarget;
        static SolidColorBrush solidColorBrush;

        static Stopwatch stopwatch;

        private static byte[] array = new byte[10000000];
        private static uint gridWidth = 1280;
        private static uint gridHeight = 800;
        private static uint gridScaleX = 1;
        private static uint gridScaleY = 1;
        private static uint maxiter = 1255;
        private static uint maxChunkSize = 128;
        private static uint g = 50;
        private static uint b = 0;
        private static uint r = 0;
        private static uint[] colorMap = new uint[255];
        private static float precision = 5000;
        private static float xMin = -2;
        private static float xMax = 0.5f;
        private static float yMin = -1;
        private static float yMax = 1;


        public Form1()
        {
            InitializeComponent();
            //form = new RenderForm("SharpDX - Fractal Test");
            //form.Load += form_Load;
            desc = new SwapChainDescription()
            {
                BufferCount = 1,
                ModeDescription =
                    new ModeDescription(FractalBox.Width, FractalBox.Height,
                                        new Rational(60, 1), Format.R8G8B8A8_UNorm),
                IsWindowed = true,
                OutputHandle = FractalBox.Handle,
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput
            };
            Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.BgraSupport, new SharpDX.Direct3D.FeatureLevel[] { SharpDX.Direct3D.FeatureLevel.Level_10_0 }, desc, out device, out swapChain);

            d2dFactory = new SharpDX.Direct2D1.Factory();

            int width = FractalBox.Width;
            int height = FractalBox.Height;


            // Ignore all windows events
            factory = swapChain.GetParent<Factory>();
            factory.MakeWindowAssociation(FractalBox.Handle, WindowAssociationFlags.IgnoreAll);

            // New RenderTargetView from the backbuffer
            backBuffer = Texture2D.FromSwapChain<Texture2D>(swapChain, 0);
            renderView = new RenderTargetView(device, backBuffer);

            surface = backBuffer.QueryInterface<Surface>();


            d2dRenderTarget = new RenderTarget(d2dFactory, surface,
                                                            new RenderTargetProperties(new PixelFormat(Format.Unknown, AlphaMode.Premultiplied)));

            solidColorBrush = new SolidColorBrush(d2dRenderTarget, Color.White);
            createColorMap();
            stopwatch = new Stopwatch();
            stopwatch.Start();
            RenderLoop.Run(this, () =>
            {
             Thread.Sleep(10);
            d2dRenderTarget.BeginDraw();
            d2dRenderTarget.Clear(Color.Black);

            // solidColorBrush.Color = new Color4(1, 1, 1, (float)Math.Abs(Math.Cos(stopwatch.ElapsedMilliseconds * .001)));
            draw(true);

            d2dRenderTarget.EndDraw();

            swapChain.Present(0, PresentFlags.None);
            });

            // Release all resources
            renderView.Dispose();
            backBuffer.Dispose();
            device.ImmediateContext.ClearState();
            device.ImmediateContext.Flush();
            device.Dispose();
            device.Dispose();
            swapChain.Dispose();
            factory.Dispose();
        }


        private static bool outOfTime(bool fast, DateTime start)
        {
            if (!fast) return false;
            var now = DateTime.Now;
            return ((now - start).TotalMilliseconds > 30);
        }

        private static int randomIntBetween(int min, int max)
        {
            return new Random().Next(min, max);
        }

        private static bool same_neighbors(uint x, uint y, uint l)
        {
            if (y < l) return false;
            if (x < l) return false;
            uint c1 = array[(y - l) * gridWidth + (x - l)];
            uint c2 = array[(y) * gridWidth + (x - l)];
            if (c1 != c2) return false;
            uint c3 = array[(y + l) * gridWidth + (x - l)];
            if (c1 != c3) return false;
            uint c4 = array[(y - l) * gridWidth + (x)];
            if (c1 != c4) return false;
            uint c5 = array[(y + l) * gridWidth + (x)];
            if (c1 != c5) return false;
            uint c6 = array[(y - l) * gridWidth + (x + l)];
            if (c1 != c6) return false;
            uint c7 = array[(y) * gridWidth + (x + l)];
            if (c1 != c7) return false;
            uint c8 = array[(y + l) * gridWidth + (x + l)];
            if (c1 != c8) return false;
            return true;
        }

        private static uint escapeIteration(float x, float y)
        {
            var x0 = x;
            var y0 = y;
            var y2 = y * y;
            var x2 = x * x;
            var iter = 0;
            while ((x2 + y2 < 4) && (iter < maxiter))
            {
                y = 2 * x * y + y0;
                x = x2 - y2 + x0;
                x2 = x * x;
                y2 = y * y;
                ++iter;
            }
            return (uint)iter;
        }


        private static uint mapColor(uint val)
        {
            r = ((r >> 4) & 15) | ((r << 4) & 240);
            g = g - val;
            b = b - val;
            return (r << 16) | (g << 8) | b;
        }

        private static void createColorMap()
        {
            colorMap = new uint[256];
            for (var i = 0; i < 256; i++)
            {
                colorMap[i] = mapColor((uint)i);
            }
        }


        private static void draw(bool fast = false)
        {
            precision = xMin;

            uint color = 0;
            uint parent_x;
            uint parent_y;
            array = new byte[10000000];

            var startTime = DateTime.Now;
            var mask = (2 * maxChunkSize) - 1;
            for (uint chunkSize = maxChunkSize; chunkSize > 0 && (!outOfTime(fast, startTime)); chunkSize >>= 1)
            {
                var yInc = (yMax - yMin) * chunkSize / gridHeight;
                var xInc = (xMax - xMin) * chunkSize / gridWidth;
                var x0 = xMin;
                var y0 = yMin;
                float x = 0;
                float y = 0;
                var chunk2 = 2 * chunkSize;
                var rect = new Rectangle(0, 0, (int)chunkSize, (int)chunkSize);
                for (uint yPixel = 0; yPixel < gridHeight; yPixel += chunkSize)
                {
                    x0 = xMin;
                    parent_y = yPixel;

                    if (Convert.ToBoolean(yPixel & mask))
                    {
                        parent_y -= chunkSize;
                    }
                    uint pyg = parent_y * gridWidth;
                    uint ypg = yPixel * gridWidth;
                    for (uint xPixel = 0; xPixel < gridWidth; xPixel += chunkSize)
                    {
                        x = x0;
                        y = y0;
                        parent_x = xPixel;

                        if (Convert.ToBoolean(xPixel & mask))
                        {
                            parent_x -= (uint)-chunkSize;
                        }
                        var do_eval = true;
                        if (chunkSize == maxChunkSize)
                        {
                            do_eval = true;
                        }
                        else if ((xPixel == parent_x) && (yPixel == parent_y))
                        {
                            do_eval = false;
                        }
                        else if (same_neighbors(parent_x, parent_y, chunk2))
                        {
                            do_eval = false;
                        }

                        if (do_eval)
                        {
                            color = escapeIteration(x0, y0) % 256;
                        }
                        else
                        {
                            color = array[pyg + parent_x];
                        }

                        array[ypg + xPixel] = (byte)color;
                        rect.Y = (int)yPixel;
                        rect.X = (int)xPixel;
                        //data.fillRect(rect, colorMap[color]);


                        try
                        {
                            var brush = new SolidColorBrush(d2dRenderTarget, new Color4(colorMap[color]));
                            var rectangle = new RectangleGeometry(d2dFactory, new Rectangle() { X = rect.X, Y = rect.Y, Height = rect.Height, Width = rect.Width });
                            //    Debug.WriteLine(colorMap[color]);
                            //var rectangle = new RoundedRectangleGeometry(d2dFactory, new RoundedRectangle() { RadiusX = 5, RadiusY = 5, Rect = new RectangleF(rect.X, rect.Y, rect.Width, rect.Height) });

                            d2dRenderTarget.FillGeometry(rectangle, brush, null);
                        }
                        catch { }

                        x0 += xInc;
                    }
                    y0 += yInc;
                }
                mask >>= 1;
            }
        }
    }
}
